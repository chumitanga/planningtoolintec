package be.intecbrussel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanningtoolfrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlanningtoolfrontendApplication.class, args);
    }

}

