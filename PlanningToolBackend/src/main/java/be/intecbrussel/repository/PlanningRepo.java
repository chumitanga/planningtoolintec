package be.intecbrussel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanningRepo extends CrudRepository<Object,Integer> {

}
